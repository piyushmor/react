//bootstrapping

//bind react to real DOM

import React from "react";
import { render } from "react-dom";

import Routes from "./app/Routes";
import "whatwg-fetch";
import store from "./app/Store";
import { Provider } from "react-redux";
//mount react component to DOM

render(<Provider store={store}>
    <Routes>
    </Routes>
</Provider>, document.getElementById("root"));