
import React from "react";
import PropTypes from "prop-types";

export default function Cart(props) {
    let list = props.items
        .map(item => {
            return (<li key={item.id} >
                {item.name}
            </li>)
        });
    //<button className="warning" onClick={(e) => { props.actions.removeItemFromCart(item.id) }}>remove<button>
    return (


        <div>
            <h2>Cart</h2>
            <ul>
                {list}
            </ul>

            <button className="error" onClick={() => { props.actions.emptyCart() }}>Empty</button>

        </div>
    )
}


Cart.defaultProps = {

}

Cart.propTypes = {

}