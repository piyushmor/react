import React from "react";

import Header from "./components/Header";
import Footer from "./components/Footer";

import Home from "./components/Home";
import About from "./components/About";
import Contact, { Address } from "./components/Contact";
import PropTypes from "prop-types";

export default class App extends React.Component {


    render() {
        return (
            <div>
                <h2>React App</h2>
                <Header>
                </Header>
                <div><h1>Main Content</h1></div>
                <div>
                    {this.props.children}
                </div>
                <Footer year="2017"></Footer>
            </div>
        );
    }

    getChildContext() {
        return {
            address: {
                city: "Pune",
                state: "Maharashtra"
            }
        }
    }
}



App.childContextTypes = {
    address: PropTypes.shape({
        city: PropTypes.string,
        state: PropTypes.string
    })
}