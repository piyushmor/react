import React from "react";
import { Route, Switch } from "react-router-dom";

import ProductLayout from "./components/ProductLayout";
import ProductSearch from "./components/ProductSearch";
import ProductEdit from "./containers/ProductEdit";
import ProductList from "./containers/ProductList";

export default function Routes(props){
    return (
        <ProductLayout>
            <Switch>
                <Route path="/products/list" component={ProductList}></Route>
                <Route path="/products/edit/:id" component={ProductEdit}></Route>
                <Route path="/products/search" component={ProductSearch}></Route>
            </Switch>
            </ProductLayout>
    )
}