
import React from "react";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";

export default function ProductLayout(props) {
    return (
        <div>
            <h2>ProductLayout</h2>
            <div>
                <NavLink to="/products/list" className="button" activeClassName="success">List</NavLink>&nbsp;
                <NavLink to="/products/search" className="button" activeClassName="success">Search</NavLink>&nbsp;
                <NavLink to="/products/edit" className="button" activeClassName="success">Edit</NavLink>&nbsp;
            </div>
            <div>
                {props.children}
            </div>
        </div>
    )
}


ProductLayout.defaultProps = {

}

ProductLayout.propTypes = {

}