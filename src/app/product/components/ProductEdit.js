
import React, { Component } from "react";
import PropTypes from "prop-types";

export default class ProductEdit extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {

        let id = this.props.match.params['id'];



        if (id) {
            this.props.actions.getProduct(id);
        }
        this.props.actions.getBrands();
    }


    render() {


        let options = this.props.brands.map(brand => (
            <option key={brand.id} value={brand.id}
            >
                {brand.name}
            </option>
        ));


        return (
            <div>
                <h2>Product Edit - {this.props.product.name}</h2>

                <form onSubmit={(e) => this.saveProduct(e)}  >
                    <input name="name"
                        value={this.props.product.name}
                        onChange={(e) => this.onValueChange(e)} />

                    <input name="year"
                        value={this.props.product.year}
                        onChange={(e) => this.onValueChange(e)} />

                    <select name="brandId"
                        onChange={(e) => this.onValueChange(e)}
                        value={this.props.product.brandId}
                    >
                        {options}
                    </select>

                    <button type="submit">Save</button>

                </form>
                    <button onClick={()=>this.gotoUrl()}>Go To URL</button>



            </div>
        )
    }

    saveProduct(e) {
        //prevent default submit

        e.preventDefault();
        this.props.actions.saveProduct(this.props.product);

    }

    gotoUrl(){
        this.props.history.push("/");
    }

    onValueChange(e) {
        let { name, value } = e.target;
        console.log(name, value);

        this.props.actions.updateProduct(Object.assign({}, this.props.product, { [name]: value }));
    }
}


ProductEdit.defaultProps = {

}

ProductEdit.propTypes = {

}