import React from "react";
import store from "../Store";
export default class Home extends React.Component {

    constructor(props) {
        super(props);

    }

    render() {
        console.log("value", store.getState().counter)
        return (
            <div>
                <h2>Home likes: {store.getState().counter} </h2>
            </div>
        )
    }

    // shouldComponentUpdate(nextProps, nextState){

    //     return this.state.likes!=nextState.likes;

    // }

    componentDidMount() {
        this.handle = setInterval(() => {
            store.dispatch({
                type: "INCR",
                value: 1
            })
        }, 2000)

        this.unsub = store.subscribe(()=>{
            console.log("subs called")
            this.forceUpdate();
        })
    }

    componentWillUnmount() {
        clearInterval(this.handle);
        this.unsub();
    }
}