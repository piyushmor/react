import React from "react";
import Contact from "./Contact";


export default class About extends React.Component {



    constructor(props) {
        super(props);
        this.state = {
            members: ["Member 1", "Member 2", "Member 3"],
            show: true,
            name: 'Enter a Value'
        }
    }

    addMember(mem) {

        // this.state.members.push(this.inputElement.value);
        // this.forceUpdate();
        this.setState({
            members: [...this.state.members, this.state.name]
        })
        console.log("add member");
    }

    onChange(e) {
        this.setState({ name: e.target.value })

    }

    toggle(e) {
        this.setState({
            show: !this.state.show
        });
    }

    render() {
        console.log("render");
        let list = this.state.members.map((m, i) => <li key={i}>{m}</li>)
        return (
            <div>
                <h2>About</h2>
                <button onClick={(e) => this.toggle(e)} >{this.state.show ? "Hide" : "Show"}</button>

                <input name="name"
                    value={this.state.name}
                    onChange={(e) => this.onChange(e)}
                />

                <button onClick={(e) => this.addMember(e)}>Add member</button>

                {this.state.show && (<ul>
                    {list}
                </ul>)}
                <Contact></Contact>
            </div>
        )
    }

    componentDidMount() {
        // this.inputElement.focus();
    }
}