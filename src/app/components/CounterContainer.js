import { connect } from "react-redux";

import Counter from "./Counter";

import { bindActionCreators } from "redux";

//mapping
//state is result of store.getState()
let mapReduxStateToReactProps = function (state) {
    //return props to component
    return {
        counter: state.counter
    }
}

let incrAction = function (value) {
    return {
        type: "INCR",
        value: value
    };
};

let mapDispatchToProps = function (dispatch) {
    return {
        // increment: function (value) {
        //     dispatch(incrAction(value));
        // }
        increment: bindActionCreators(incrAction, dispatch)

    }
}

let connectFn = connect(mapReduxStateToReactProps, mapDispatchToProps)

let CounterContainerComponent = connectFn(Counter);



export default CounterContainerComponent;