import React from "react";

import { NavLink } from "react-router-dom";

export default class Header extends React.Component {

    //Life cycle
    //Step 1: Cons
    //called only time
    constructor(props) {
        super(props);
        console.log("header created ", props.title);
    }

    //Step 2:
    //Real DOM/ View is not ready
    //called only time
    componentWillMount() {
        console.log("componentWillMount")
    }

    //Step 3: 
    //returns a view
    //returns virtual dom
    //called many times
    render() {
        console.log("render called")
        return (
            <div>
                <h2>{this.props.title}</h2>

                <div>
                    <NavLink to="/" exact className="button" activeClassName="success" >Home</NavLink>&nbsp;

                    <NavLink to="/products" className="button" activeClassName="success" >Products</NavLink>&nbsp;

                    <NavLink to="/counter" className="button" activeClassName="success">Counter</NavLink>&nbsp;

                    <NavLink to="/about" className="button" activeClassName="success">About</NavLink>&nbsp;
                    <NavLink to="/contact" className="button" activeClassName="success">Contact</NavLink>&nbsp;
                    <NavLink to="/cart" className="button" activeClassName="success">Cart</NavLink>&nbsp;
                </div>
            </div>
        )
    }

    //Step 4: 
    //Real DOM ready
    //The rendered view displayed on real dom
    //View ready
    //called only once
    componentDidMount() {
        console.log("componentDidMount ")
    }
}

Header.defaultProps = {
    title: 'My Header'
}