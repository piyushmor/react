import React from "react";

export default class Home extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            likes: 0
        }

        this.counter = 0;
    }

    render() {
        console.log("value", this.state.likes)
        return (
            <div>
                <h2>Home likes: {this.state.likes}</h2>
            </div>
        )
    }

    // shouldComponentUpdate(nextProps, nextState){

    //     return this.state.likes!=nextState.likes;

    // }

    componentDidMount() {
        this.handle=setInterval(() => {
            this.counter++;
            let roundValue = Math.ceil(this.counter / 10);
            //console.log("value",roundValue);
            if (roundValue != this.state.likes) {
                this.setState({ likes: roundValue })
            }
        }, 500)
    }

    componentWillUnmount(){
        clearInterval(this.handle);
    }
}