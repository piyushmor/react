import React from "react";

export default function Footer(props) {
    return (
        <div>
            <hr />
            <p>Copyrights {props.year}</p>
        </div>
    )
}

Footer.defaultProps = {
    year: "2017"
}