import React from "react";
import PropTypes from "prop-types";

export default class Contact extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <h2>Contact</h2>
                <button onClick={(e) => this.props.handler(e)}>Contact Now</button>
                <Address></Address>
            </div>
        )
    }
}

Contact.propTypes = {
    handler: PropTypes.func
}

export class Address extends React.Component {



    render() {
        return (
            <div>
                <h2>Address</h2>
                <span>{this.context.address.city}, {this.context.address.state}</span>
            </div>
        )
    }
}

Address.contextTypes = {
    address: PropTypes.shape({
        city: PropTypes.string,
        state: PropTypes.string
    })
}