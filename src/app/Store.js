import {
    createStore,
    combineReducers, applyMiddleware
} from "redux";

import thunk from "redux-thunk";

import cartReducer from "./cart/CartReducer";
//reducers are functions
//accepts and return output value

const INITIAL_STATE = 0;

let storage = window.sessionStorage;

function counterReducer(state = INITIAL_STATE,
    action) {
    console.log("counterReducer", state, action);

    switch (action.type) {
        case "INCR":
            return state + action.value;

        case "DECR":
            return state - action.value;

        default:
            return state;
    }
}

import productReducer from "./product/Reducer";

let rootReducer = combineReducers({
    counter: counterReducer,
    cartState: cartReducer,
    productState: productReducer

})



function cartMiddleware(store) {

    return function (next) {
        return function (action) {
            console.log(" cartMiddleware");

            var result = next(action)
            //reducer excuted
            let state = store.getState();
            if (action.type.indexOf("CART") >= 0) {
                storage.setItem("carts", JSON.stringify(state.cartState));
            }
            return result;
        }
    }
}

let cartItems = [];
if (storage.carts) {
    cartItems = JSON.parse(storage.carts);
}
//store shall maintain state/value returned by reducers
let store = createStore(rootReducer, { cartState: cartItems },
    applyMiddleware(thunk, cartMiddleware))


store.subscribe(() => {
    console.log("SUB ", store.getState());
});


//action creator helpers
function createIncrementAction(value) {
    return {
        type: 'INCR',
        value: value
    }
}

function asyncActionCreator() {
    return function (dispatch) {
        console.log("***called by thunk")

        dispatch(createIncrementAction(10000));
    }
}

let actionFunc = asyncActionCreator();

store.dispatch(actionFunc);

export default store;



//IGNORE BELOW CODE
//dispatch an action
let action = {
    type: 'INCR',
    value: 10
}

//store internally calls reducer
//reducers give return value
//store keep the latest value
store.dispatch(action)

//get state returns last knwon value from store
console.log("Result ", store.getState());

action = {
    type: 'DECR',
    value: 5
}

store.dispatch(action);
console.log("Result ", store.getState());

let unknown = {
    type: 'XYZ',
    test: 'hello'
}

store.dispatch(unknown);
console.log("Result ", store.getState());



action = {
    type: 'DECR',
    value: 2
}




action = createIncrementAction(4);

store.dispatch(action);
console.log("Result ", store.getState());


