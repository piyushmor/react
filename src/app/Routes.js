import React from "react";
import {
    BrowserRouter as Router,
    Route,
    Switch
} from "react-router-dom";
import Home from "./components/Home";
import Contact from "./components/Contact";
import About from "./components/About";
import App from "./App";
import NotFound from "./components/NotFound";
import ProductRoutes from "./product/Routes";
import Cart from "./cart/containers/Cart";
import Counter from "./components/CounterContainer";

export default function Routes(props) {
    return (
        <Router>
            <App>
                <Switch>
                    <Route path="/" exact component={Home} >
                    </Route>
                    <Route path="/about" component={About} >
                    </Route>
                    <Route path="/contact" component={Contact} >
                    </Route>
                    <Route path="/counter" component={Counter} >
                    </Route>
                    <Route path="/products">
                        {ProductRoutes}
                    </Route>
                    <Route path="/cart" component={Cart} >
                    </Route>
                    <Route path="*" component={NotFound} >
                    </Route>
                </Switch>
            </App>
        </Router>
    )
}